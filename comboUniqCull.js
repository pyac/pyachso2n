#!/bin/bash

"use strict";
const files = ["comboUniqList-core.txt", "comboUniqList-mid.txt", 
      "comboUniqList-mega.txt"];
//const files = ["comboUniqList-mega.txt"];//, "comboUniqList-mid.txt", 
//      "comboUniqList-mega.txt"];
//const files = ["sample.txt"];
var io = require("../../lib/io");
const fs = require('fs');
const nearnessConstant = 0.7;
const natural = require('natural');
const stemmer = natural.PorterStemmer;
//const stemmer = natural.LancasterStemmer;
//const englishSuffixes = [
//"sploitation", "a-palooza", "cephalous", "dermatous", "graphical", "iatrician",
//"ification", "istically", "plinerved", "something", "ambulist", "anthropy",
//"centesis", "centrism", "cephalic", "fication", "gnathous", "mageddon",
//"morphism", "morphous", "oic acid", "phoresis", "tainment", "year-old",
//"ability", "acharya", "andrian", "androus", "angular", "blastic", "borough",
//"carpous", "centric", "cephaly", "ception", "dromous", "enchyma", "escence",
//"facient", "faction", "genesis", "grapher", "iatrics", "ibility", "iferous",
//"isation", "istical", "ization", "kinesis", "licious", "lingual", "logical",
//"malacia", "manship", "meister", "morphic", "o-matic", "odontia", "oecious",
//"ologist", "onomics", "partite", "phagous", "philiac", "philous", "phrenia",
//"poiesis", "polises", "pounder", "preneur", "pterous", "rrhagia", "rrhaphy",
//"rrhexis", "strophy", "tacular", "thermal", "thermic", "trophic", "tropism",
//"ylidene", "a-thon", "aceous", "acious", "adelic", "adenia", "agogue", "aholic",
//"amundo", "aneous", "athlon", "bility", "biosis", "biotic", "burger", "cardia",
//"carpic", "chezia", "choron", "chrome", "clinal", "clinic", "coccus", "colous",
//"cratic", "dipsia", "ectomy", "ennial", "escent", "ferous", "furter", "gamous",
//"geddon", "genous", "gerous", "gramme", "graphy", "hedral", "hedron", "holism",
//"iatric", "illion", "imundo", "landia", "leptic", "logist", "logues", "mancer",
//"maniac", "mantic", "megaly", "mentum", "merous", "metric", "mobile", "morphy",
//"mycete", "nomics", "o-rama", "ocracy", "oholic", "ometer", "ometry", "ostomy",
//"parous", "pathic", "people", "person", "phagia", "phasia", "philia", "philic",
//"phobia", "phobic", "phonic", "plasia", "plasty", "plegia", "plegic", "pteran",
//"ridden", "riffic", "rrhoea", "saurus", "scopic", "selves", "sexual", "sphere",
//"splain", "stasis", "static", "statin", "tastic", "thermy", "trophy", "tropic",
//"uretic", "valent", "vorous", "worthy", "zygous", "acity", "aemia", "agogy",
//"algia", "ality", "amine", "ander", "andry", "angle", "archy", "arian", "arium",
//"assed", "aster", "astic", "athon", "ation", "ative", "atory", "biont", "blast",
//"bound", "burgh", "caine", "chore", "chory", "cidal", "clase", "clast", "cline",
//"cocci", "coele", "cracy", "craft", "crasy", "crete", "cycle", "derma", "diene",
//"drome", "dynia", "elect", "ergic", "esque", "fugal", "genic", "genin", "gonal",
//"graph", "grave", "hedra", "henge", "holic", "iasis", "iatry", "ician", "icide",
//"icism", "icity", "idine", "ienne", "iform", "ismus", "istic", "itude", "izzle",
//"lalia", "latry", "lepsy", "lings", "logic", "logue", "loquy", "lysis", "lytic",
//"machy", "mance", "mancy", "mania", "meter", "metre", "metry", "micin", "morph",
//"mycin", "nasty", "odont", "ogony", "ology", "omata", "omics", "onium", "onomy",
//"onymy", "opsia", "orama", "osity", "otomy", "pants", "pathy", "pause", "pedia",
//"penia", "petal", "phage", "phagy", "phile", "phily", "phobe", "phone", "phony",
//"phore", "phyll", "phyte", "plast", "pnoea", "poeia", "polis", "proof", "ptile",
//"rices", "rrhea", "rrhœa", "sauce", "scape", "scope", "scopy", "shire", "sicle",
//"sophy", "speak", "stock", "stomy", "style", "styly", "terol", "therm", "tinib",
//"to-be", "topia", "topic", "treme", "trope", "troph", "tropy", "tuple", "verse",
//"ville", "wards", "woman", "women", "worth", "ylene", "zilla", "able", "ably",
//"acal", "adic", "agog", "aire", "algy", "ally", "amic", "ance", "ancy", "anth",
//"arch", "atim", "ator", "born", "boro", "burg", "bury", "cade", "care", "carp",
//"cele", "cene", "chan", "cide", "coel", "core", "corn", "crat", "cyte", "derm",
//"elle", "emia", "ence", "ency", "enyl", "eous", "ergy", "esce", "etic", "ette",
//"exia", "fest", "fier", "fold", "ford", "form", "free", "gamy", "gasm", "gate",
//"gaze", "geny", "glot", "gony", "gram", "gyny", "head", "hood", "iana", "ible",
//"ibly", "ical", "icle", "iety", "ific", "inda", "iour", "ious", "ista", "itis",
//"itol", "izer", "kin-", "kind", "kini", "kins", "land", "lect", "lept", "less",
//"like", "ling", "lite", "lith", "lock", "logs", "logy", "long", "mane", "mans",
//"meal", "ment", "mere", "mony", "more", "most", "nado", "naut", "nazi", "nema",
//"ness", "nomy", "nymy", "oate", "omas", "onym", "opia", "opsy", "osin", "osis",
//"otic", "path", "pexy", "phil", "phor", "phyl", "plex", "pnea", "pœia", "poly",
//"pter", "punk", "R Us", "rama", "ress", "safe", "sama", "saur", "self", "ship",
//"side", "sies", "sion", "soft", "some", "sson", "stan", "stat", "ster", "tard",
//"teen", "tene", "thon", "tide", "tion", "tome", "tomy", "tort", "town", "tron",
//"tude", "type", "uous", "uret", "urgy", "uria", "vir-", "vore", "ward", "ware",
//"ways", "wear", "wich", "wick", "wide", "wise", "work", "xeny", "zoan", "zoic",
//"ade", "age", "all", "ana", "and", "ane", "ant", "ard", "ary", "ase", "ass",
//"ast", "ate", "bie", "bot", "cha", "cin", "dar", "dom", "ean", "een", "eer",
//"eme", "end", "ene", "ent", "'er", "ern", "ers", "ery", "ese", "ess", "est",
//"eth", "fag", "fix", "ful", "gen", "gon", "ial", "ian", "ica", "ice", "ick",
//"ics", "ide", "ier", "ies", "ify", "ile", "ily", "in'", "ine", "ing", "ïng",
//"ino", "ion", "iot", "ise", "ish", "ism", "ist", "ite", "ity", "ium", "ive",
//"ize", "kin", "kun", "let", "lin", "log", "lol", "mab", "man", "mas", "max",
//"men", "mer", "nap", "nik", "nom", "n't", "nym", "ock", "ode", "off", "oic",
//"oid", "ola", "ole", "oma", "ome", "one", "oon", "ory", "ose", "oth", "ous",
//"oxy", "oyl", "ple", "pod", "'re", "red", "rel", "ren", "ric", "rix", "san",
//"sis", "ski", "sky", "sol", "son", "ton", "ual", "ule", "ure", "wad", "way",
//"xor", "yer", "yne", "zza", "ab", "ac", "ad", "ae", "al", "an", "ar", "ay",
//"by", "ce", "cy", "'d", "ed", "èd", "ee", "eh", "el", "en", "er", "es", "et",
//"ex", "ey", "fu", "fy", "i-", "ia", "ic", "id", "ie", "in", "ja", "ji", "le",
//"ly", "'m", "mo", "nd", "oi", "ol", "on", "or", "os", "ov", "rd", "ry", "'s",
//"st", "th", "ty", "um", "yl"];

//const englishSuffixes = [ "escence", "ization", "oholic", "ectomy", "iatric", "logist", "ostomy", "phobia", "plegia", "plegic", "scribe", "script", "sophic", "trophy", "ocity", "algia", "arian", "arium", "orium", "ation", "ative", "cracy", "cycle", "esque", "gonic", "iasis", "ation", "loger", "ology", "otomy", "pathy", "phile", "phone", "phyte", "scopy", "scope", "sophy", "able", "ade ", "ance", "cide", "crat", "cule", "emia", "ence", "ency", "etic", "ette", "gamy", "hood", "ible", "ical", "ious", "less", "like", "ling", "ment", "ness", "onym", "opia", "opsy", "osis", "path", "pnea", "sect", "ship", "sion", "tion", "tome", "tomy", "tude", "ular", "uous", "ward", "ware", "wise", "age", "ian", "ant", "ary", "ate", "dom", "dox", "eer", "ent", "ern", "ese", "ess", "est", "ful", "gam", "gon", "ial", "ian", "ile", "ily", "ine", "ing", "ion", "ish", "ism", "ist", "ite", "ity", "ive", "ize", "let", "log", "oid", "oma", "ory", "ous", "ure", "ac", "al", "cy", "ed", "ee", "en", "er", "fy", "ic", "ly", "or", "th", "ty" ];

const exceptionList = ["pod", "skin", "demonstrate", "barium", "civilization", "personality", "animated", "respectively", "applications", "nominal", "secretion", , "ration", "containers", "organisms", "ironing", "relativity", "modulate", "conductivity", "animism", "doctorate", "computability", "admiral", "equations", "humane", "universities", "literate",  "training", "objection", "author", "escape", "glance", "prison", "software", "hardware", "recycle", "easter", "vanish", "anatomy", "projectile", "sealing", "radial", "curator", "cosmology", "bisect", "fluorescence", "header", "radiant", "hostess", "bowing", "homeward", "howling", "dawned", "palmer", "crackling", "deserter", "organist", "cloven", "inductive", "textile", "votive", "alkaloid", "telepathy", "sanitarium", "indent", "plutocracy", "canonization", "stereoscope", "theosophy", "realty", "sadism", "microscopy", "internationalization", "fielding", "hospitality", "passive", "import", "variable", "printer", "mustard", "sweater", "leaflet", "adapter", "feminist", "processor",   "tailor", "flourish", "refresh", "directory" ];


//const exceptionList = [ "" ];

const englishSuffixes = [ "escence", "ization", "oholic", "ectomy", "iatric", 
      "logist", "ostomy", "phobia", "plegia", "plegic", "scribe", "script", 
      "sophic", "trophy", "ocity", "algia", "arian", "arium", "orium", "ation", 
      "cracy", "cycle", "esque", "gonic", "iasis", "ation", "loger", 
      "ology", "otomy", "pathy", "phile", "phone", "phyte", "scopy", "scope", 
      "sophy", "able", "ade ", "ance", "cide", "crat", "cule", "emia", "ence", 
      "ator",
      "ency", "etic", "ette", "gamy", "hood", "ible", "ical", "ious", "less", 
      "like", "ling", "ment", "ness", "onym", "opia", "opsy", "osis", "path", 
      "pnea", "sect", "ship", "sion", "tion", "tome", "tomy", "tude", "ular", 
      "uous", "ward", "wise", "age", "ian", "ant", "ard", "ate", "dom", 
      "dox", "eer", "ent", "ern", "ese", "ess", "est", "ful", "gam", "gon", 
      "ial", "ian", "ile", "ily", "ine", "ing", "ion", "ish", "ism", "ist", 
      "ite", "ity", "ive", "ize", "let", "log", "oid", "oma", "ous", 
      "ure", "ac", "cy", "ed",  "en", "er", "fy", "ic", "ly", 
      "or", "ty" ];
//const englishSuffixes=[ "ance", "ator", "tion", "ion", "ant", "er", "or", "ed"];

function stringToWordLines(string) {
  function lineToWords(line) {
    return line.split(" ");
  }
  var lines = string.split("\n"),
  wordLines = lines.map(lineToWords);
  // console.log(lines);
  return wordLines;
}

function wordOfEachLine(wordIndex, wordLines) {
  return wordLines.map(function (line) {
    return line[wordIndex];
  });
}

function removeSuffix(word, suffixList) {
  var suffix = "";
  for (var i = 0; i < suffixList.length; i++) {
    suffix = suffixList[i]; 
    if (word.length < suffix.length +1) continue;
    if (word.substring(word.length - suffix.length) == suffix) 
      return word.substring(0, word.length - suffix.length);
  }
  return word;
}
const blacklistedRegex = [];

function isBlacklisted(word) {
  return  blacklistedRegex.some(re => re.test(word));
}

function nearDuplicateCompare(example, comparison) {
  // return false if blacklisted 
  if (isBlacklisted(example)) return false; 
  //console.log(example + " " + comparison);
  if (exceptionList.indexOf(example) != -1) return false;
  var long = (Math.max(example.length, comparison.length)* nearnessConstant);
  //if (example.length < long || comparison.length < long) { return false; }
  var exampleSubstring = stemmer.stem(example); //example.substring(0,long);
  var comparisonSubstring = stemmer.stem(comparison);//comparison.substring(0,long); 
  var stemmerResult = exampleSubstring.localeCompare(comparisonSubstring) == 0;
  var pstemmerResult = example.localeCompare(comparisonSubstring)
    == 0 || exampleSubstring.localeCompare(comparison) == 0;
  // console.log("ES " + exampleSubstring + " " + comparisonSubstring);
  exampleSubstring = removeSuffix(example, englishSuffixes); 
  comparisonSubstring = removeSuffix(comparison, englishSuffixes);
  var desuffixedResult =  exampleSubstring.localeCompare(comparisonSubstring) == 0;
  var pdesuffixedResult =  exampleSubstring.localeCompare(comparison) == 0 ||
    example.localeCompare(comparisonSubstring) == 0 ;
   //console.log("DS " + exampleSubstring + " " + comparisonSubstring + " "  +
//exampleSubstring.localeCompare(comparison) + " " +
 //      + pdesuffixedResult);
  // console.log("   " + example + " " + comparison );
  return (stemmerResult  || pstemmerResult || desuffixedResult || pdesuffixedResult? 
      true : false);
  }

function previousNearDuplicate(word, index, array) {
  //return true;
  // return new Promise((resolve, reject) => {                                
  if (word.length*nearnessConstant <= 4) return false;
  var workingArray =  array.slice(0,  index );
  //console.log(workingArray + " " + workingArray.length + " workingArrray " + index + " a " + word );
  // resolve(
  //   true
  var duplicates = workingArray.filter(function(comparison) { 
    return nearDuplicateCompare(word, comparison);
  })
  if (duplicates.length > 0) 
    console.log("duplicates :" + duplicates + " " + word);
  var result = (duplicates.length == 0 ? false : true);
  //console.log("result =" + result);
  return result;

  // );
  //});
}


function readFilePromise(filename) {                                             
  return new Promise((resolve, reject) => {                                
    fs.readFile(filename, 'utf8', function (err, data) {             
      if (err) reject (err);                                   
      resolve(data);                                           
    });                                                              
  });                                                                      
}   

function writeFilePromise(filename, text) {                                      
  console.log("writing file " + text);                                     
  return new Promise((resolve, reject) => {                                
    fs.writeFile(filename, text, function(err) {                     
      if (err) reject(err);                                    
      resolve(0);                                              
    });                                                              
  });                                                                      
}  

function outFormat(outLines){
  return outLines.map((line) => {
    return line[0] + (line[1] ? " " + line[1]:"") + (line[2] ? " " + line[2]:"") +"\n";
  });
}


function removeNearDuplicates(filename) {
  var wordLines = [];
  var mainWords = [];
  var blacklist = [];
  readFilePromise(filename)
    .catch((rejection) => {console.log("no file " + filename + " " + rejection)})
    .then((fileContents) => {return stringToWordLines(fileContents)})
    .then((lineWords) => {
      wordLines = lineWords; 
      return wordOfEachLine(0, wordLines)})
    .then((wordsMain) => {
      mainWords = wordsMain; 
      console.log(wordLines.length + " wordLines.length " + mainWords.length);
      /*console.log(JSON.stringify(mainWords));*/})
  .then(() => {
    return new Promise((resolve, reject) => {
      resolve(mainWords.filter(function(word, index, array) {
	return (previousNearDuplicate(word, index, array));
      })); })})
  .then((blacklistP) => {
    blacklist = blacklistP;
    return wordLines.filter(function(wordLine) {
      return (blacklist.indexOf(wordLine[0]) == -1);
    })}).
    then((outLines) => {
      var outForm =  outFormat(outLines).join("");
  //console.log(
  //    outForm);
  io.fileWrite(filename+"1.txt", outForm);
    });
  // print output file from wordLines
}

function main() {
  files.forEach(function(filename) {
    removeNearDuplicates(filename);
  });
}

main();
