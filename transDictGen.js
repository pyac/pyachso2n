#!/usr/bin/node
////////////////////////////////////////////////////////////////
//          0x10            0x20            0x30            0x40
//3456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0
//      10        20        30        40        50        60  64
//34567890123456789012345678901234567890123456789012345678901234
////////////////////////////////////////////////////////////////
/// be file sh for generate new word for concept ya
/// su speakable programming for every language be title ya
/// su la AGPL-3 be license ya
/// be end of head ya
///
"use strict";
var io = require("../../lib/io"),
  langWords = JSON.parse(io.fileRead("langWords-mega.json")),
  transObjX = JSON.parse(io.fileRead("genTransX.json")),
  uniqueObjX = JSON.parse(io.fileRead("unique-mega.json")),
  blacklist = uniqueObjX.blacklist,
  thesaurus = uniqueObjX.thesaurus,
  rootList = langWords.rootList,
  gramList = langWords.gramList;


function formatSuggestList(thesaurus, blacklist, word) {
    function approvedWords(word) {
        if (thesaurus["X" + word] !== undefined) {
            return true;
        }
        return false;
    }
    var bentry = blacklist[word];
  //console.log("bentry " + JSON.stringify(bentry));
    var result = [];
    if (Array.isArray(bentry)) {
        result = (bentry.filter(approvedWords));
    }
    console.log("result " + JSON.stringify(result));
    return result;
}

function translateWord(transObjX, language_code, word) {
  if (word[0] != 'X') { word = "X" + word; }
  if (language_code && word && transObjX) {
  return transObjX[word] && transObjX[word][language_code] && 
                   transObjX[word][language_code].replace(/[ \-]/g,"_");
  } else {
    console.log("language_code " + language_code);
    console.log("word " + word);
    console.log("transObjX " + transObjX);
  }
  return;

}
function produce_dictionary(language_code) {
  var dictionary = {};
  var pyash_dativeCase = {};
  var pyash_ablativeCase = {};
  var regional_blacklist = {};
  var translation = "";
  var cardinalWords = Object.keys(rootList);
  var blacklistWords = Object.keys(blacklist);
  //console.log("cw " +  cardinalWords);
  if (language_code != "py") {
  cardinalWords.forEach(function (word) {
    translation = transObjX[word][language_code] && 
                  transObjX[word][language_code].replace(/[ \-]/g,"_");
    if (!translation) { return; }
    if (gramList[word]) {
      pyash_dativeCase["X" + translation] = rootList[word][0];
      pyash_dativeCase["X_" + translation] = gramList[word][0];
      if(!pyash_dativeCase["X_" + translation.substring(0,3)]) {
        pyash_dativeCase["X_" + translation.substring(0,3)] = gramList[word][0];
        pyash_ablativeCase["X" + gramList[word][0]] = "_" +
                    translation.substring(0,3);
      } else {
        pyash_dativeCase["X_" + translation] = gramList[word][0];
        pyash_ablativeCase["X" + gramList[word][0]] = "_" + translation;
      }
      pyash_ablativeCase["X" + rootList[word][0]] = translation;
      console.log("gl " + gramList[word][0] + " " + translation);
    } else {
      pyash_dativeCase["X" + translation] = rootList[word][0];
      pyash_ablativeCase["X" + rootList[word][0]] = translation;
      console.log("rl " + rootList[word][0] + " " + translation);
    }
  });
  } else {
    Object.keys(rootList).forEach(function (word) {
      let tlat = rootList[word][0];
      pyash_dativeCase["X" + tlat] = tlat;
      pyash_ablativeCase["X" + tlat] = tlat;
    });
    Object.keys(gramList).forEach(function (word) {
      let tlat = gramList[word][0];
      pyash_dativeCase["X" + tlat] = tlat;
      pyash_ablativeCase["X" + tlat] = tlat;
    });
  }
  if (language_code !== "pya") {
  blacklistWords.forEach(function (word) {
    let suggestList = formatSuggestList(thesaurus, blacklist, word);
    translation = translateWord(transObjX, language_code, word);
    suggestList = suggestList.map(function(word) {
      console.log("word " +word);
      return translateWord(transObjX, language_code, word);
    });
    regional_blacklist["X" + translation] = suggestList;
    console.log("bt " + word  + " " + translation + " " + 
      JSON.stringify(suggestList));
  });
  }
  dictionary.pyash_dativeCase = pyash_dativeCase;
  dictionary.pyash_ablativeCase = pyash_ablativeCase;
  dictionary.blacklist = regional_blacklist;
  return dictionary;
}

function cardinal(langCode) {
  var dictionary = {};
  dictionary[langCode] = produce_dictionary(langCode);
  io.fileWrite("dictionary_"+ langCode+ ".json",JSON.stringify(dictionary));
}

cardinal(process.argv[2]);
